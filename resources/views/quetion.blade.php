@extends('master.master')

@section('title', 'Quetion')
@section('nav-active2', 'active')

@section('container')

    <section class="container header">
    <h1 class="mx-auto">All Quetion</h1>
    </section>
    <section class="container quetion">

        @foreach($topics as $topic)

        <div class="quetion row flex-column mt-3 bg-light rounded p-4" >
            <div class="card">
                <div class="card-body">
                    
                    <h5 class="card-title"><a href="/home/quetion/{{$topic->topic_id*5}}">{{$topic->subject}}</a></h5>
                    <!-- <p class="card-text">{{$topic->message}}</p> -->
                    <p class="card-text"><?= $topic->message ?></p>
                    <span>Category</span>
                    <span class='btn btn-primary p-1'>{{$topic->category_name}}</span>
                    <div class="row justify-content-end mr-5">
                        <p class="card-link ml-3">{{$topic->created_at}}</p>
                        <p class="card-link text-primary ml-3">{{$topic->username}}</p>
                    </div>
                </div>
            </div>
        </div>

        @endforeach

    </section>

    <section class="container form-quetion mt-5 mb-5">
        <form action="/home/quetion" method="POST">
            @csrf
            <div class="form-group">
                <select name='category_id' class="form-control">
                    @foreach($categories as $category)
                        <option value='{{$category->category_id}}' class="form-control">{{$category->category_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                    <input type="text" name="subject" placeholder="Subject" required class="form-control">
            </div>
            
            <div class="form-group">
                <textarea name="message" id="" cols="30" rows="4" class="form-control"></textarea>
            </div>

            @if (Route::has('login'))
                
                    @auth
                        <div class="d-flex justify-content-end">
                            <input type="submit" class="btn btn-primary" value="Send Quetion">
                        </div>
                    @else
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('login') }}" class="btn btn-primary">Login if you want ask</a>
                        </div>
                    @endauth
                
            @endif


        </form>
   
    </section>

    <script>
        CKEDITOR.replace( 'message' );
    </script>
@endsection

