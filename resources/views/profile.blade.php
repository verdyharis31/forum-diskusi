@extends('master.master')

@section('title', 'Profile')
@section('nav-active3', 'active')

@section('container')
    <section class="container pt-5">
        <div class="row d-flex justify-content-end">
             <h5 class="mr-2"> Point (<span> {{$user->point}} </span>)</h5>
        </div>

        <div class="row mt-5">
            <!-- <div class="col border border-dark">
                <img src="" alt="" heigt=200px class="">
            </div> -->
            <div class="col p-5">
                <h3>{{$user->username}}</h3>
            </div>
        </div>

    </section>
@endsection