@extends('master.master')

@section('title', 'Quetion')

@section('container')


    <section class="container answer mt-5">

        <div class="answer row flex-column mt-3 bg-light rounded p-4" >
            
                <h3>{{$topics->subject}}</h3>
                <p><?=$topics->message?></p>

                <p class="text-sm text-primary">{{$topics->username}}</p>
        </div>

    </section>

    <section class="container">

        <div class="row justify-content-end">

            @foreach($comments as $comment)
            <div class="answer col-10 flex-column mt-3 bg-light rounded p-4" >
                <p><?=$comment->comment?></p>

                <div class="row justify-content-end mr-5">
                    <p class="card-link text-dark">{{$comment->created_at}}</p>
                    <p class="card-link text-primary">{{$comment->username}}</p>

                @if (Route::has('login'))
                    @auth

                   <?php $user = auth()->user();
                        if($user->username = $topics->username){ ?>
                             <a href="/edit/point/{{$comment->username}}" class="btn btn-primary ml-3" > <i class="far fa-hand-point-up"></i> Add point </a>
                   <?php    } ?>
                   @else
                   @endauth
                @endif
                   
                </div>
            </div>
            @endforeach
        </div>

    </section>

    <section class="container form-answer mt-5 mb-5">
        <form action="/home/quetion/answer" method="POST">
            @csrf
            
            <div class="form-group">
                <textarea name="comment" id="" cols="30" rows="4" class="form-control" required></textarea>
            </div>
                <input type="hidden" name="topic_id"  value={{$topics->topic_id}} >

            @if (Route::has('login'))
                
                    @auth
                        <div class="d-flex justify-content-end">
                            <input type="submit" class="btn btn-primary" value="Send Your Answer">
                        </div>
                    @else
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('login') }}" class="btn btn-primary">Login if you want Answer</a>
                        </div>
                    @endauth
                
            @endif
        </form>
   
    </section>

    <script>
        CKEDITOR.replace( 'comment' );
    </script>
@endsection

