@extends('master.master')

@section('title', 'home')
@section('nav-active1', 'active')


@if (Route::has('login'))
    <div class="top-right links">
        @auth
            <!-- <a href="{{ url('/home') }}">Home</a> -->
            @else
                <!-- <a href="{{ route('login') }}">Login</a> -->

            @if (Route::has('register'))
                <!-- <a href="{{ route('register') }}">Register</a> -->
            @endif
        @endauth
    </div>
@endif

@section('container')
    <section class="container">
        <div class="row banner pt-5 pb-5">
            <div class="col">
            <h1 class="mt-5">Berbagi adalah cara terbaik untuk mengajarkan ilmu</h1>
            </div>
            <div class="col">
                <img src="{{ ('image/picbanner.png') }}" alt="">
            </div>
        </div>
    </section>

    <div class="bg-light">
        <div class="container">

            <div class="row">

                @foreach($categories as $category)
                <div class="col-6 col-lg-3 rounded p-3 d-flex align-items-stretch "> 
                    <div class="flex-column  p-2" style="width:16rem; height:10rem; overflow:auto; box-shadow: 2px 2px 8px rgba(0,0,0,0.1);" >
                        <a class="btn btn-dark rounded p-1" href="#">{{$category->category_name}}</a>
                        <p>{{$category->information}}</p>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection