<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Comment;
use App\Topic;
use App\User;
use DB;

class UserController extends Controller
{
    public function home(){
       $categories = Category::all();
       return view('index',['categories'=>$categories]);
    }

    public function quetion(){
      $topics = DB::table('topics')
         ->join('categories', 'categories.category_id', '=', 'topics.category_id')
         ->join('users', 'users.id', '=', 'topics.user_id')
         ->get();

      $categories = Category::all();
       return view('quetion',['topics'=>$topics,'categories'=>$categories]);
    }


    public function profile(){
       $user = auth()->user();
       return view('profile',['user'=>$user]);
    }


    public function showAnswer($topic_id){
       $topic_id = $topic_id / 5;
      //  $topic = DB::table('topics')->where('topic_id', $topic_id)->first();
      $que = 'select * from topics, users where topic_id = '.$topic_id.' and topics.user_id = users.id';
      $topic = DB::select($que);
      

      $query = 'select comments.comment, users.id, users.username, topics.topic_id, comments.created_at 
                from comments, users, topics
                where comments.user_id = users.id and comments.topic_id = topics.topic_id and comments.topic_id = '.$topic_id .' ';

      $comments = DB::select($query);

       return view('answer',['topics'=>$topic[0], 'comments'=>$comments]);
    }

    public function insert(Request $request){
      $user = auth()->user();
      // dd($user);
      $new_topic = new Topic();
      $new_topic->category_id = $request["category_id"];
      $new_topic->subject = $request["subject"];
      $new_topic->message = $request["message"];
      $new_topic->user_id = $user->id;
      $new_topic->save();



      $topics = DB::table('topics')
      ->join('categories', 'categories.category_id', '=', 'topics.category_id')
      ->join('users', 'users.id', '=', 'topics.user_id')
      ->get();

      $categories = Category::all();
      return view('quetion',['topics'=>$topics,'categories'=>$categories]);

    }

    public function insertAnswer(Request $request){
      $user = auth()->user();
      $new_comment = new Comment();
      $new_comment->topic_id = $request["topic_id"];
      $new_comment->comment = $request["comment"];
      $new_comment->user_id = $user->id;
      $new_comment->save();

      $topic_id = $request["topic_id"];
      
      $topic = DB::table('topics')->where('topic_id', $topic_id)->first();

      $query = 'select comments.comment, users.id, users.username, topics.topic_id, comments.created_at 
                from comments, users, topics
                where comments.user_id = users.id and comments.topic_id = topics.topic_id and comments.topic_id = '.$topic_id .' ';

      $comments = DB::select($query);

       return view('answer',['topics'=>$topic, 'comments'=>$comments]);
    }

    public function updatePoint($username){
       $query = "select * from users where username = '".$username."'";
       $tables = DB::select($query);
      //  $tabl = $table['point'];
      foreach($tables as $table){
         $data = $table->point;
      }
      $data = $data + 15;

       $pointUpdate = DB::table('users')
              ->where('username', $username)
              ->update(['point' => $data]);

      $user = auth()->user();
      return view('profile',['user'=>$user]); 
    }
}

