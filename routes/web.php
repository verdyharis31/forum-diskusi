<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::get('/', 'UserController@home');
Route::get('/home/profile', 'UserController@profile');
Route::get('/home/quetion', 'UserController@quetion');
Route::post('/home/quetion', 'UserController@insert');
Route::get('/home/quetion/{topic_id}','UserController@showAnswer');
Route::post('/home/quetion/answer','UserController@insertAnswer');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/edit/point/{username}','UserController@updatePoint');
